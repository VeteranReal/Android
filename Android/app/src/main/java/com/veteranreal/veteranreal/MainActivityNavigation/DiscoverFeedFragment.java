package com.veteranreal.veteranreal.MainActivityNavigation;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.veteranreal.veteranreal.R;

public class DiscoverFeedFragment extends Fragment {

    public static DiscoverFeedFragment newInstance() {

        Bundle args = new Bundle();

        DiscoverFeedFragment fragment = new DiscoverFeedFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.discover_feed_layout, container, false);
    }
}
