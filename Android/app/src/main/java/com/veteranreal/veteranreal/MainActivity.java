package com.veteranreal.veteranreal;

import android.app.FragmentManager;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.veteranreal.veteranreal.HelperClasses.*;
import com.veteranreal.veteranreal.MainActivityNavigation.DiscoverFeedFragment;
import com.veteranreal.veteranreal.MainActivityNavigation.HomeFeedFragment;
import com.veteranreal.veteranreal.MainActivityNavigation.NewsFeedFragment;
import com.veteranreal.veteranreal.MainActivityNavigation.NotificationFragment;
import com.veteranreal.veteranreal.MainActivityNavigation.ProfileFragment;

import android.util.Log;
import android.view.MenuItem;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ActionBar toolBar = getSupportActionBar();
        BottomNavigationView BNV = findViewById(R.id.navigationView);
        BottomNavigationViewHelper.removeShiftMode(BNV);

        getFragmentManager().beginTransaction()
                .replace(R.id.FrameLayout_main, HomeFeedFragment.newInstance())
                .commit();

        BNV.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                if (item.getItemId() == R.id.navigation_home){
                    Log.e("NavItem","Home Clicked");

                    getFragmentManager().beginTransaction()
                            .replace(R.id.FrameLayout_main, HomeFeedFragment.newInstance())
                            .commit();

                    return true;
                }else if (item.getItemId() == R.id.navigation_discover){
                    Log.e("NavItem","Discover Clicked");
                    getFragmentManager().beginTransaction()
                            .replace(R.id.FrameLayout_main, DiscoverFeedFragment.newInstance())
                            .commit();
                    return true;
                }else if (item.getItemId() == R.id.navigation_news){
                    Log.e("NavItem","News Clicked");
                    getFragmentManager().beginTransaction()
                            .replace(R.id.FrameLayout_main, NewsFeedFragment.newInstance())
                            .commit();
                    return true;
                }else if (item.getItemId() == R.id.navigation_notifications){
                    Log.e("NavItem","Notifications Clicked");
                    getFragmentManager().beginTransaction()
                            .replace(R.id.FrameLayout_main, NotificationFragment.newInstance())
                            .commit();
                    return true;
                }else if (item.getItemId() == R.id.navigation_profile){
                    Log.e("NavItem","Profile Clicked");
                    getFragmentManager().beginTransaction()
                            .replace(R.id.FrameLayout_main, ProfileFragment.newInstance())
                            .commit();
                    return true;
                }

                 return false;
            }
        });

    }
}


