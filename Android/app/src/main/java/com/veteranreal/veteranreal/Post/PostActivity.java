package com.veteranreal.veteranreal.Post;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.veteranreal.veteranreal.R;

public class PostActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);


        //ALPHA CODE --------- WILL BE REPLACED UPON THE ADDITION OF FIREBASE DATABASE FOR POST CREATION

        Button post = findViewById(R.id.button_create_post);

        final EditText title = findViewById(R.id.editText_post_title);
        final EditText location = findViewById(R.id.editText_post_location);
        final EditText description = findViewById(R.id.editText_post_description);

        post.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("Post Activity","Post Clicked! Title: " + title.getText() + " Location: " + location.getText() + " description: " + description.getText());
                Intent resultIntent = new Intent();
                resultIntent.putExtra("Title", title.getText().toString());
                resultIntent.putExtra("Location", location.getText().toString());
                resultIntent.putExtra("Description", description.getText().toString());
                setResult(Activity.RESULT_OK, resultIntent);
                finish();
            }
        });


    }





}
