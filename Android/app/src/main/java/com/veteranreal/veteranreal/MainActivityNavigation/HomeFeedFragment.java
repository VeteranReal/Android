package com.veteranreal.veteranreal.MainActivityNavigation;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;

import com.veteranreal.veteranreal.MainActivity;
import com.veteranreal.veteranreal.Post.PostActivity;
import com.veteranreal.veteranreal.R;

import java.util.ArrayList;

public class HomeFeedFragment extends Fragment {

    ArrayList<String> TempPost = null;
    ListView tempPostList = null;

    private static final int CREATE_POST_INTENT = 0x001;

    public static HomeFeedFragment newInstance() {

        Bundle args = new Bundle();

        HomeFeedFragment fragment = new HomeFeedFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.home_feed_layout, container, false);

        TempPost = new ArrayList<>();

        tempPostList = rootView.findViewById(R.id.homefeed);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, TempPost);

        tempPostList.setAdapter(adapter);

        return rootView;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.menu_button_create_post){
            Log.e("HomeFeedFragment","Post Button Tapped!");
            Intent intent = new Intent(getActivity(), PostActivity.class);
            startActivityForResult(intent, CREATE_POST_INTENT); //TODO: CHANGE THIS TO START ACTIVITY WITHOUT RESULT WHEN FIREBASE IS IMPLEMENTED INTO POST CREATION.
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.home_feed_menu, menu);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode){
            case CREATE_POST_INTENT:{
                if (resultCode == Activity.RESULT_OK) {
                    String title = data.getStringExtra("Title");
                    // TODO Update your TextView.

                    TempPost.add(title);

                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, TempPost);
                    tempPostList.setAdapter(adapter);

                }
                break;
            }
        }

    }

    class HomeFeedAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return 0;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return null;
        }
    }

}
