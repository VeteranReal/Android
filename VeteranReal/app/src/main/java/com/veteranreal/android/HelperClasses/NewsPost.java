package com.veteranreal.android.HelperClasses;

import java.io.Serializable;

public class NewsPost implements Serializable {

    private String mTitle;
    private String mDescription;
    private String mTimeStamp;

    public NewsPost(String mTitle, String mTimeStamp, String mDescription) {
        this.mTitle = mTitle;
        this.mTimeStamp = mTimeStamp;
        this.mDescription = mDescription;
    }

    public String getmTitle() {
        return mTitle;
    }

    public String getmTimeStamp() {
        return mTimeStamp;
    }

    public String getmDescription() {
        return mDescription;
    }
}
