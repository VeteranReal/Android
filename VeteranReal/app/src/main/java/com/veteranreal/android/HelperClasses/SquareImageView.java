package com.veteranreal.android.HelperClasses;

/*
 * This class was created with the intentions to create a uniform looking gridview. The solution was achieved by
 * the help of a user from stack overflow in the following post:
 *
 *  https://stackoverflow.com/questions/15261088/gridview-with-two-columns-and-auto-resized-images?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa
 *
 * All credit to: "Kcoppock" @ https://stackoverflow.com/users/321697/kcoppock
 *
 */






import android.content.Context;
import android.util.AttributeSet;

public class SquareImageView extends android.support.v7.widget.AppCompatImageView {
    public SquareImageView(Context context) {
        super(context);
    }

    public SquareImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SquareImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(getMeasuredWidth(), getMeasuredWidth()); //Snap to width
    }
}


