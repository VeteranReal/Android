package com.veteranreal.android.NewsDetailActivity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.veteranreal.android.HelperClasses.NewsPost;
import com.veteranreal.android.R;

import org.w3c.dom.Text;

public class NewsDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.news_detail_layout);

        NewsPost currentNews = (NewsPost) getIntent().getSerializableExtra("currentNews");

        if (currentNews != null){

            TextView title = findViewById(R.id.TextView_news_details_title);
            TextView description = findViewById(R.id.textView_news_details_description);

            title.setText(currentNews.getmTitle());
            description.setText(currentNews.getmDescription());

        }

    }
}
