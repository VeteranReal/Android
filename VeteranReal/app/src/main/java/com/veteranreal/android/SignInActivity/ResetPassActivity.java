package com.veteranreal.android.SignInActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.veteranreal.android.R;

public class ResetPassActivity extends AppCompatActivity {

    EditText email;
    ImageView resetPass;
    FirebaseAuth mAuth;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.password_reset_layout);

        TextView signUp = findViewById(R.id.textView_dont_have_account);
        email = findViewById(R.id.editText_change_password_email);

        resetPass = findViewById(R.id.imageView_change_button);

        mAuth = FirebaseAuth.getInstance();

        resetPass.setOnClickListener(new ImageView.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (email.getText().length() > 0){

                    mAuth.sendPasswordResetEmail(email.getText().toString())
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        Log.d("TAG", "Email sent.");
                                        Toast.makeText(ResetPassActivity.this, "Email Sent!", Toast.LENGTH_SHORT).show();
                                    }else if (!task.isSuccessful()){
                                        Toast.makeText(ResetPassActivity.this, "Email Is Not Tied To An Account!", Toast.LENGTH_SHORT).show();
                                    }
                                }

                            });

                }else{
                    Toast.makeText(ResetPassActivity.this, "Email Field Cant be Left Blank!", Toast.LENGTH_SHORT).show();
                }

            }
        });


        signUp.setOnClickListener(new TextView.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signUpIntent = new Intent(ResetPassActivity.this, SignUpActivity.class);
                startActivity(signUpIntent);
            }
        });
    }
}


