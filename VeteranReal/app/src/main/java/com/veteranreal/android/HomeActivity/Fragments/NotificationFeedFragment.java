package com.veteranreal.android.HomeActivity.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.veteranreal.android.HelperClasses.Notification;
import com.veteranreal.android.HelperClasses.Post;
import com.veteranreal.android.PostDetailActivity.CreateCommentActivity;
import com.veteranreal.android.PostDetailActivity.PostDetailActivity;
import com.veteranreal.android.R;
import com.veteranreal.android.UserProfileActivity.UserProfileActivity;

import java.util.ArrayList;
import java.util.Objects;

public class NotificationFeedFragment extends Fragment {

    ListView notificationFeed;
    ArrayList<Notification> feed;
    ArrayList<String> followers;
    FirebaseAuth mAuth;
    CustomAdapter adapter;


    public static NotificationFeedFragment newInstance() {

        Bundle args = new Bundle();

        NotificationFeedFragment fragment = new NotificationFeedFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.notification_feed_layout, container, false);

        notificationFeed = rootView.findViewById(R.id.listView_notifications);

        feed = new ArrayList<>();

        mAuth = FirebaseAuth.getInstance();

        loadNotifications();

        adapter = new CustomAdapter(getContext(), R.layout.notification_feed_cell, feed);

        notificationFeed.setOnItemClickListener(new ListView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent userProfileIntent = new Intent(getContext(), UserProfileActivity.class);
                userProfileIntent.putExtra("currentUser", feed.get(position));
                startActivity(userProfileIntent);

            }
        });



        return rootView;
    }


    private void loadNotifications(){

        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference ref = database.getReference("followers").child(Objects.requireNonNull(mAuth.getCurrentUser()).getUid());


        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot snapshot: dataSnapshot.getChildren()){

                    //followers.add(snapshot.getKey());


                    //if (snapshot.child("type").getValue().toString().equals("follow")){

                        Log.e("TAG", "Found a Follow");

                        if (snapshot.getKey() != null){

                            final DatabaseReference userRef = database.getReference("users").child(snapshot.getKey());

                            userRef.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                    Log.e("TAG",dataSnapshot.child("fullname").getValue().toString() + " is the follower");

                                    feed.add(new Notification(dataSnapshot.child("fullname").getValue().toString(), userRef.getKey(), "Follow"));

                                    Log.e("TAG", "Current Notification User Reference UUID is " + userRef.getKey());

                                    notificationFeed.setAdapter(adapter);

                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });


                        }

                    //}

                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        //here


    }

    class CustomAdapter extends ArrayAdapter<Notification>
    {

        private int listItemLayout;

        public CustomAdapter(Context context, int layoutId, ArrayList<Notification> array)
        {
            super(context, layoutId, array);

            listItemLayout  = layoutId;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {


            final Notification currentItem = getItem(position);
            final ViewHolder vh;

            if(convertView == null)
            {
                vh = new ViewHolder();
                LayoutInflater inflater = LayoutInflater.from(getContext());
                convertView = inflater.inflate(listItemLayout, parent, false);

                //inflate custom layout
                vh.followText = convertView.findViewById(R.id.textView_notification_text);

                convertView.setTag(vh);
            }
            else
            {
                vh = (ViewHolder) convertView.getTag();
            }

            vh.followText.setText(String.format("%s Has followed you!", Objects.requireNonNull(currentItem).getmName()));


            return convertView;
        }

        private class ViewHolder
        {
            TextView followText;
            ImageView profile_image;
        }

    }


}
