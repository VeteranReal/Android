package com.veteranreal.android.HomeActivity.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.veteranreal.android.R;

import java.io.IOException;
import java.util.Objects;
import java.util.UUID;

import static android.app.Activity.RESULT_OK;

public class PostCreationFragment extends Fragment {

    PostCreationListener mListener;
    private String author;
    private Bitmap profileImage;
    private EditText description, location;
    private int PICK_IMAGE_REQUEST = 71;
    private Uri filePath;
    private ImageView postImage;
    private Boolean customPostPicture = false;

    private FirebaseStorage storage;
    private StorageReference storageReference;

    private FirebaseAuth mAuth;

    public static interface PostCreationListener{
        public void sharePost();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof PostCreationListener){
            mListener = (PostCreationListener) context;
        }

    }

    public static PostCreationFragment newInstance() {

        Bundle args = new Bundle();

        PostCreationFragment fragment = new PostCreationFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.create_post_layout, container, false);

        ImageView shareButton = rootView.findViewById(R.id.imageView_post_creation_share_button);

        android.support.v7.widget.Toolbar toolbar = rootView.findViewById(R.id.toolbar_New_Post);

        ImageView selectPhoto = (ImageView) toolbar.findViewById(R.id.imageView_toolbar_message_hub_button);

        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();

        selectPhoto.setOnClickListener(new ImageView.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
            }
        });

        description = rootView.findViewById(R.id.editText_Post_Description);

        location = rootView.findViewById(R.id.editText_Post_Location);

        postImage = rootView.findViewById(R.id.imageView_Post_Image);

        mAuth = FirebaseAuth.getInstance();


        // Get a reference to our Users
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference ref = database.getReference("users").child(mAuth.getCurrentUser().getUid());

        // Attach a listener to read the data at our posts reference
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Log.e("TAG", dataSnapshot.child("fullname").getValue().toString());

                author = Objects.requireNonNull(dataSnapshot.child("fullname").getValue()).toString();


//                try {
//                    InputStream in = new java.net.URL(Objects.requireNonNull(dataSnapshot.child("profileImageUrl").getValue()).toString()).openStream();
//
//                    Log.e("TAG", Objects.requireNonNull(dataSnapshot.child("profileImageUrl").getValue()).toString());
//
//                    Glide.with(getContext()).load(Objects.requireNonNull(dataSnapshot.child("profileImageUrl").getValue()).toString())
//                            .into(postImage);
//
//
//                    profileImage = BitmapFactory.decodeStream(in);
//                } catch (Exception e) {
//                    Log.d("Error", e.getStackTrace().toString());
//                }
//
//                postImage.setImageBitmap(profileImage);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });

        shareButton.setOnClickListener(new ImageView.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (customPostPicture){
                    if (description.getText().toString().length() >= 5){
                        if (location.getText().toString().length() >= 5 && location.getText().toString().contains(",")){
                            Log.e("TAG", "Passed all Validation Fields!");
                            uploadImage();
                        }else{
                            Log.e("TAG", "Location not longer than 5 characters or does not contain ,");
                            Toast.makeText(getContext(), "Location must have a \",\" and be 5 characters or longer", Toast.LENGTH_SHORT).show();
                        }
                    }else {
                        Log.e("TAG", "Description not longer than 5 characters");
                        Toast.makeText(getContext(), "Description must be 5 characters or longer!", Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Log.e("TAG", "Post Image not Added");
                    Toast.makeText(getContext(), "Must add an Image to Post!", Toast.LENGTH_SHORT).show();
                }


            }
        });

        return rootView;
    }


    private void uploadImage() {

        if (filePath != null) {
            final ProgressDialog progressDialog = new ProgressDialog(getContext());
            progressDialog.setTitle("Uploading...");
            progressDialog.show();

            final UUID postIdentifier = UUID.randomUUID();

            final StorageReference ref = storageReference.child("post_image_test/" + "-LN" + postIdentifier);
//            firebaseImageString = ref.getDownloadUrl().toString();

            //Log.e("TAG" , "image url: " + ref.getDownloadUrl());
            ref.putFile(filePath)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            progressDialog.dismiss();

                            // Get a reference to our posts
                            final FirebaseDatabase database = FirebaseDatabase.getInstance();
                            final DatabaseReference myRef = database.getReference("posts").child("-LN" + postIdentifier);


                            ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {

                                    Log.d("TAG", "onSuccess: uri= " + uri.toString());

                                    myRef.child("caption").setValue(description.getText().toString());
                                    myRef.child("likeCount").setValue(0);
                                    myRef.child("uid").setValue(mAuth.getCurrentUser().getUid());
                                    myRef.child("location").setValue(location.getText().toString());
                                    myRef.child("postUUID").setValue("-LN" + postIdentifier.toString());

                                    myRef.child("photoUrl").setValue(uri.toString());
                                    mListener.sharePost();
                                }
                            });

                            //Toast.makeText(getContext(), "Post Created", Toast.LENGTH_SHORT).show();

                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Toast.makeText(getActivity(), "Failed " + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot
                                    .getTotalByteCount());
                            progressDialog.setMessage("Uploaded " + (int) progress + "%");
                        }
                    });
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK
                && data != null && data.getData() != null )
        {
            filePath = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), filePath);
                postImage.setImageBitmap(bitmap);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            customPostPicture = true;
        }
    }

}
