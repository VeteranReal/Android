package com.veteranreal.android.HomeActivity.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.common.util.IOUtils;
import com.veteranreal.android.HelperClasses.NewsPost;
import com.veteranreal.android.NewsDetailActivity.NewsDetailActivity;
import com.veteranreal.android.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.net.ssl.HttpsURLConnection;

public class NewsFeedFragment extends Fragment {

    private ListView newsFeedListView;
    private ArrayList<NewsPost> newsPostArray;
    private NewsAdapter adapter;

    public static NewsFeedFragment newInstance() {

        Bundle args = new Bundle();

        NewsFeedFragment fragment = new NewsFeedFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.news_feed_layout, container, false);

        newsPostArray = new ArrayList<>();

        newsFeedListView = rootView.findViewById(R.id.listView_newsFeed);

        newsFeedListView.setOnItemClickListener(new ListView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //        ImageView news = rootView.findViewById(R.id.imageView_news_feed);
//
//        news.setOnClickListener(new ImageView.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//            }
//        });

                Intent newsDetailIntent = new Intent(getActivity(), NewsDetailActivity.class);
                newsDetailIntent.putExtra("currentNews", newsPostArray.get(position));
                startActivity(newsDetailIntent);

            }
        });

         adapter = new NewsAdapter(Objects.requireNonNull(getContext()), R.layout.news_feed_cell_layout, newsPostArray);

         DataTask task = new DataTask();
         task.execute();

        newsFeedListView.setAdapter(adapter);

        return rootView;
    }

    private class DataTask extends AsyncTask<Void, Void, ArrayList<NewsPost>>{

        @Override
        protected ArrayList<NewsPost> doInBackground(Void... strings) {

            try{

                HttpsURLConnection con = null;

                URL u = new URL("https://www.reddit.com/r/VeteransBenefits/.json");

                con = (HttpsURLConnection) u.openConnection();

                con.connect();

                InputStream is = con.getInputStream();

                String dataString = org.apache.commons.io.IOUtils.toString(is);
                is.close();

                JSONObject object = new JSONObject(dataString);
                JSONObject people = object.getJSONObject("data");
                JSONArray children = people.getJSONArray("children");

                for (int i = 0; i < children.length(); i++){
                    JSONObject innerObject = (JSONObject) children.get(i);
                    JSONObject RedditData = innerObject.getJSONObject("data");
                    String title = RedditData.getString("title");
                    String timeStamp = String.valueOf(RedditData.getInt("created_utc"));
                    String description = RedditData.getString("selftext");
                    newsPostArray.add(new NewsPost(title, timeStamp, description));
                }

            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(ArrayList<NewsPost> newsPosts) {
            super.onPostExecute(newsPosts);

            newsFeedListView.setAdapter(adapter);

        }
    }

    private class NewsAdapter extends ArrayAdapter<NewsPost> {

        private int listItemLayout;

        public NewsAdapter(@NonNull Context context, int resource, @NonNull List<NewsPost> objects) {
            super(context, resource, objects);

            listItemLayout = resource;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

            final NewsPost post = getItem(position);
            final ViewHolder vh;

            if (convertView == null){
                vh = new ViewHolder();
                LayoutInflater inflater = LayoutInflater.from(getContext());
                convertView = inflater.inflate(listItemLayout, parent, false);

                vh.title = convertView.findViewById(R.id.textView_news_article_title);

                convertView.setTag(vh);
            }else{
                vh = (ViewHolder) convertView.getTag();
            }

            vh.title.setText(post.getmTitle());

            return convertView;
        }

        private class ViewHolder
        {
            TextView title, timestamp;
        }
    }
}
