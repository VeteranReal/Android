package com.veteranreal.android.SignInActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.veteranreal.android.HomeActivity.HomeActivity;
import com.veteranreal.android.R;

public class SignInActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signin_signin_layout);

        ImageView signIn = findViewById(R.id.imageView_sign_in_button_signin);
        final EditText email = findViewById(R.id.editText_email_signin);
        final EditText password = findViewById(R.id.editText_password_signin);
        mAuth = FirebaseAuth.getInstance();

        signIn.setOnClickListener(new ImageView.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (email.getText().toString().length() > 0 && password.getText().toString().length() > 0){
                    validateUser(email.getText().toString(), password.getText().toString());
                }


            }
        });

        TextView signUp = findViewById(R.id.textView_no_account);

        signUp.setOnClickListener(new TextView.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signUpIntent = new Intent(SignInActivity.this, SignUpActivity.class);
                startActivity(signUpIntent);
            }
        });

    }

    private void validateUser(String email, String password){
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("TAG", "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();

                            Intent homeActivityIntent = new Intent(SignInActivity.this, HomeActivity.class);
                            startActivity(homeActivityIntent);

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("TAG", "signInWithEmail:failure", task.getException());
                            Toast.makeText(SignInActivity.this, "Authentication failed " + task.getException().getLocalizedMessage(),
                                    Toast.LENGTH_SHORT).show();
                        }

                        // ...
                    }
                });
    }

}
