package com.veteranreal.android.HomeActivity.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.veteranreal.android.HelperClasses.Post;
import com.veteranreal.android.HomeActivity.HomeActivity;
import com.veteranreal.android.PostDetailActivity.PostDetailActivity;
import com.veteranreal.android.ProfileSettingsActivity.ProfileSettingsActivity;
import com.veteranreal.android.R;
import com.veteranreal.android.UserProfileActivity.ProfileFollowScreen;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Objects;

public class ProfileFragment extends Fragment {

    TextView name, branch, description, posts, followers, following;
    ImageView profileImage, posts_button, followers_button, following_button;
    GridView grid;
    GridAdapter adapter;
    ArrayList<Post> profilePostArray;
    FirebaseAuth mAuth;

    public static ProfileFragment newInstance() {

        Bundle args = new Bundle();

        ProfileFragment fragment = new ProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.my_profile_layout, container, false);

        mAuth = FirebaseAuth.getInstance();
        profilePostArray = new ArrayList<>();

         name = rootView.findViewById(R.id.textView_profile_user_name);
         branch = rootView.findViewById(R.id.textView_profile_military_branch);
         description = rootView.findViewById(R.id.textView_profile_description);
         profileImage = rootView.findViewById(R.id.imageView_profile_user_image);
         grid = rootView.findViewById(R.id.gridView_profile_posts);
         posts = rootView.findViewById(R.id.textView_profile_num_of_posts);
         followers = rootView.findViewById(R.id.textView_profile_num_of_followers);
         following = rootView.findViewById(R.id.textView_profile_num_of_following);
         followers_button = rootView.findViewById(R.id.imageView_profile_followers_button);
         following_button = rootView.findViewById(R.id.imageView_profile_following_button);
         adapter = new GridAdapter();
         grid.setAdapter(adapter);

        Toolbar toolbar = rootView.findViewById(R.id.toolbar_New_Post);

        ImageView settingsButton = (ImageView) toolbar.findViewById(R.id.imageView_settings_cog);

        followers_button.setOnClickListener(new ImageView.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent followersIntent = new Intent(getContext(), ProfileFollowScreen.class);
                followersIntent.putExtra("TitleFollowScreen", "Followers");
                startActivity(followersIntent);

            }
        });

        following_button.setOnClickListener(new ImageView.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent followingIntent = new Intent(getContext(), ProfileFollowScreen.class);
                followingIntent.putExtra("TitleFollowScreen", "Following");
                startActivity(followingIntent);

            }
        });

        settingsButton.setOnClickListener(new TextView.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent settingsIntent = new Intent(getContext(), ProfileSettingsActivity.class);
                startActivity(settingsIntent);

            }
        });

         loadProfile();
         loadPosts();
         loadFollows();
         loadFollowing();

        return rootView;
    }

    public void loadProfile() {
        // Get a reference to our Users
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference ref = database.getReference("users").child(mAuth.getCurrentUser().getUid());


        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                try {
                    Glide.with(Objects.requireNonNull(getContext())).load(new URL(Objects.requireNonNull(dataSnapshot.child("profileImageUrl").getValue()).toString())).into(profileImage);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }

                name.setText(Objects.requireNonNull(dataSnapshot.child("fullname").getValue()).toString());

                if (dataSnapshot.child("branch").getValue() != null){
                    branch.setText(Objects.requireNonNull(dataSnapshot.child("branch").getValue()).toString());
                }else{
                    branch.setText("Branch Not Defined");
                }

                if (dataSnapshot.child("description").getValue() != null){
                    description.setText(Objects.requireNonNull(dataSnapshot.child("description").getValue()).toString());
                }else{
                    description.setText("No Description Defined");
                }



            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void loadFollowing(){

        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference ref = database.getReference("following").child(mAuth.getCurrentUser().getUid());

        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                ArrayList<DataSnapshot> followersArray = new ArrayList<>();

                for (DataSnapshot snapshot: dataSnapshot.getChildren()){

                    followersArray.add(snapshot);

                    following.setText(String.valueOf(followersArray.size()));

                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void loadFollows(){

        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference ref = database.getReference("followers").child(mAuth.getCurrentUser().getUid());

        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                ArrayList<DataSnapshot> followersArray = new ArrayList<>();

                for (DataSnapshot snapshot: dataSnapshot.getChildren()){

                    followersArray.add(snapshot);

                    followers.setText(String.valueOf(followersArray.size()));

                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void loadPosts(){

        // Get a reference to our Users
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference ref = database.getReference("posts");

        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot snapshot: dataSnapshot.getChildren()){

                    if (Objects.requireNonNull(snapshot.child("uid").getValue()).toString().equals(Objects.requireNonNull(mAuth.getCurrentUser()).getUid())){
                        Log.e("Tag", "Found User Post! Adding to Array");


                        try {

                            URL postImage = new URL(snapshot.child("photoUrl").getValue().toString());

                            loadAuthorInformation(snapshot.child("caption").getValue().toString(),
                                    snapshot.child("location").getValue().toString(),
                                    postImage,
                                    "30 min ago",
                                    snapshot.child("postUUID").getValue().toString(),
                                    (long) snapshot.child("likeCount").getValue(), snapshot.child("uid").getValue().toString());

                            //profilePostArray.add(new Post(snapshot.child("caption").getValue().toString(), snapshot.child("location").getValue().toString(), , "", "", "", "", "", ""))
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        }
                    }

                }

                adapter.notifyDataSetChanged();

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void loadAuthorInformation(final String description, final String location, final URL postImage, final String time, final String postUUID, final long likes, final String authorUid){

        // Get a reference to our Users
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference ref = database.getReference("users").child(authorUid);

        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                try {

                    URL postProfileImage = new URL(dataSnapshot.child("profileImageUrl").getValue().toString());

                    profilePostArray.add(new Post(description, location, postImage, postProfileImage, dataSnapshot.child("fullname").getValue().toString(), time, postUUID, likes, authorUid));
                    adapter.notifyDataSetChanged();
                    posts.setText(String.valueOf(profilePostArray.size()));
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });



    }

    private class GridAdapter extends BaseAdapter {

        public GridAdapter() {

        }

        @Override
        public int getCount() {
            return profilePostArray.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            final ViewHolder holder;

            if(convertView == null){
                convertView = getLayoutInflater().inflate(R.layout.grid_view_cell_layout, parent, false);

                holder = new ViewHolder();
                holder.imageView = convertView.findViewById(R.id.imageView_cellImage);

                convertView.setTag(holder);
            }else{
                holder = (ViewHolder) convertView.getTag();
            }

            Glide.with(getContext()).load(profilePostArray.get(position).getImage()).apply(RequestOptions.centerCropTransform()).into(holder.imageView);

            holder.imageView.setOnClickListener(new ImageView.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent postDetailIntent = new Intent(getActivity(), PostDetailActivity.class);

                    postDetailIntent.putExtra("ClickedPost", profilePostArray.get(position));

                    postDetailIntent.putExtra("liked", false);

                    //postDetailIntent.putExtra("likes", likes);

                    startActivity(postDetailIntent);
                }
            });


            return convertView;
        }

    }

    private static class ViewHolder {
        ImageView imageView;
    }

}
