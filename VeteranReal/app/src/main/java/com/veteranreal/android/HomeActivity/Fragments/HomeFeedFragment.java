package com.veteranreal.android.HomeActivity.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.veteranreal.android.HelperClasses.Post;
import com.veteranreal.android.HomeActivity.HomeActivity;
import com.veteranreal.android.PostDetailActivity.CreateCommentActivity;
import com.veteranreal.android.PostDetailActivity.PostDetailActivity;
import com.veteranreal.android.R;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Objects;

public class HomeFeedFragment extends Fragment {

    HomeFeedListener mListener;
    ArrayList<Post> homeFeedPostArray;
    private String profileImageUrl;
    private ListView mHomeFeed;
    private FirebaseAuth mAuth;
    private Boolean isLiked;
    private String author;
    private long likes;

    public static interface HomeFeedListener{
        public void createPost();
        public void messageHub();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof HomeFeedListener){
            mListener = (HomeFeedListener) context;
        }

    }

    public static HomeFeedFragment newInstance() {

        Bundle args = new Bundle();

        HomeFeedFragment fragment = new HomeFeedFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        homeFeedPostArray = new ArrayList<>();

        View rootView = inflater.inflate(R.layout.home_feed_layout, container, false);

        Toolbar toolbar = rootView.findViewById(R.id.toolbar_New_Post);

        TextView postButton = (TextView) toolbar.findViewById(R.id.home_feed_layout_toolbar_post_textView);

        ImageView messageHub = (ImageView) toolbar.findViewById(R.id.imageView_toolbar_message_hub_button);

        mAuth = FirebaseAuth.getInstance();

        mHomeFeed = rootView.findViewById(R.id.listView_home_feed);

        TextView empty = rootView.findViewById(R.id.home_feed_list_view_empty);

        mHomeFeed.setEmptyView(empty);


            loadAuthorProfile();

            loadPosts();

        final SwipeRefreshLayout pullToRefresh = rootView.findViewById(R.id.swipeRefreshLayout);
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                homeFeedPostArray.clear();
                loadPosts();// your code
                pullToRefresh.setRefreshing(false);
            }
        });


//        messageHub.setOnClickListener(new ImageView.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mListener.messageHub();
//            }
//        });

        postButton.setOnClickListener(new TextView.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.createPost();
            }
        });

        return rootView;
    }

    class CustomAdapter extends ArrayAdapter<Post>
    {

        private int listItemLayout;

        public CustomAdapter(Context context, int layoutId, ArrayList<Post> posts)
        {
            super(context, layoutId, posts);

            listItemLayout  = layoutId;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {


            final Post post = getItem(position);
            final ViewHolder vh;
            isLiked = false;

            if(convertView == null)
            {
                vh = new ViewHolder();
                LayoutInflater inflater = LayoutInflater.from(getContext());
                convertView = inflater.inflate(listItemLayout, parent, false);

                //inflate custom layout
                vh.name = convertView.findViewById(R.id.textView_post_user_name);
//                vh.time = convertView.findViewById(R.id.textView_post_time);
                vh.location = convertView.findViewById(R.id.textView_post_location);
                vh.like_count = convertView.findViewById(R.id.textView_post_like_count);
                vh.descroption = convertView.findViewById(R.id.textView_post_description_cell);
//                vh.comment_count = convertView.findViewById(R.id.textView_post_comment_count);

                vh.profile_image = convertView.findViewById(R.id.imageView_post_profile_image);
                vh.post_image = convertView.findViewById(R.id.imageView_post_image);
                vh.like_image = convertView.findViewById(R.id.imageView_post_like);
                vh.comment_image = convertView.findViewById(R.id.imageView_post_comment);

                convertView.setTag(vh);
            }
            else
            {
                vh = (ViewHolder) convertView.getTag();
            }

            final FirebaseDatabase database = FirebaseDatabase.getInstance();
            final DatabaseReference postRef = database.getReference("posts").child(post.getUUID());

            postRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    for (DataSnapshot like : dataSnapshot.child("likes").getChildren()) {

                        if (Objects.requireNonNull(like.getKey()).equals(Objects.requireNonNull(mAuth.getCurrentUser()).getUid())) {
                            isLiked = true;
                            likes = (long) dataSnapshot.child("likeCount").getValue();
                            Glide.with(getContext()).load(R.drawable.like_image_active).into(vh.like_image);
                            break;
                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });


            vh.name.setText(post.getAuthor());
//            vh.name.setText(mArrayList.get(position).getLocation());
            vh.location.setText(post.getLocation());
            vh.like_count.setText(String.format("%d Likes", post.getLikes()));
            vh.descroption.setText(post.getDescription());
//            vh.comment_count.setText("0");


            vh.like_image.setOnClickListener(new ImageView.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final FirebaseDatabase database = FirebaseDatabase.getInstance();
                    final DatabaseReference postRef = database.getReference("posts").child(post.getUUID());

//                    isLiked = false;

                    postRef.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                            likes = (long) dataSnapshot.child("likeCount").getValue();

                                if (!isLiked){
                                    likes++;
                                    postRef.child("likes").child(mAuth.getCurrentUser().getUid()).setValue("true");
                                    postRef.child("likeCount").setValue(likes);
                                    vh.like_image.setImageDrawable(getResources().getDrawable(R.drawable.like_image_active));
                                    vh.like_count.setText(String.format("%s Likes", String.valueOf(likes)));
                                    isLiked = true;
                                }else{

                                    if (likes > 0){
                                        likes--;
                                    }
                                    postRef.child("likes").child(mAuth.getCurrentUser().getUid()).removeValue();
                                    vh.like_image.setImageDrawable(getResources().getDrawable(R.drawable.like));
                                    postRef.child("likeCount").setValue(likes);
                                    vh.like_count.setText(String.format("%s Likes", String.valueOf(likes)));
                                    isLiked = false;
                                }
                            }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }
            });

            vh.post_image.setOnClickListener(new ImageView.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent postDetailIntent = new Intent(getActivity(), PostDetailActivity.class);

                    postDetailIntent.putExtra("ClickedPost", getItem(position));
                    if (vh.like_image.getDrawable() == getResources().getDrawable(R.drawable.like_image_active)){
                        postDetailIntent.putExtra("liked", true);
                    }else{
                        postDetailIntent.putExtra("liked", false);
                    }

                    postDetailIntent.putExtra("likes", likes);

                    startActivity(postDetailIntent);
                }
            });

            vh.comment_image.setOnClickListener(new ImageView.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent postCommentsIntent = new Intent(getActivity(), CreateCommentActivity.class);
                    postCommentsIntent.putExtra("postUID", post.getUUID());
                    startActivity(postCommentsIntent);
                }
            });

            Glide.with(Objects.requireNonNull(getContext())).load(post.getImage()).into(vh.post_image);
            Glide.with(Objects.requireNonNull(getContext())).load(post.getProfileImage()).into(vh.profile_image);

            return convertView;
        }

        private class ViewHolder
        {
            TextView name, time, location, like_count, descroption;
            ImageView profile_image, post_image, like_image, comment_image;
        }

    }

    private void loadFollowingPosts(final String uid){

        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference("posts");

        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot snapshot: dataSnapshot.getChildren()){

                    if (Objects.requireNonNull(snapshot.child("uid").getValue()).toString().equals(Objects.requireNonNull(uid))){

                        Log.e("TAG", "Found Following Post! " + snapshot.child("caption"));

                    }

                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });



    }

    public void loadPosts(){

        //final ArrayList<String> followingUUIDs = new ArrayList<>();

        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference("posts");
        DatabaseReference followingRef = database.getReference("following").child(mAuth.getCurrentUser().getUid());

        followingRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot: dataSnapshot.getChildren()){

                    //followingUUIDs.add(snapshot.getKey());

                    loadFollowingPosts(snapshot.getKey());

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        // Attach a listener to read the data at our posts reference
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                //Log.e("TAG", dataSnapshot.toString());

                for (DataSnapshot snapshot: dataSnapshot.getChildren()){
                    if (Objects.requireNonNull(snapshot.child("uid").getValue()).toString().equals(Objects.requireNonNull(mAuth.getCurrentUser()).getUid())){
                        Log.e("TAG", "Found User Post");

                        if (profileImageUrl != null){

                            try {
                                URL postImage = new URL(Objects.requireNonNull(snapshot.child("photoUrl").getValue()).toString());
                                URL profileImage = new URL(profileImageUrl);


                                homeFeedPostArray.add(new Post(Objects.requireNonNull(snapshot.child("caption").getValue()).toString(), snapshot.child("location").getValue().toString(), postImage, profileImage, author, "30 min ago", snapshot.child("postUUID").getValue().toString(), (long) snapshot.child("likeCount").getValue(), snapshot.child("uid").getValue().toString()));

                            } catch (MalformedURLException e) {
                                e.printStackTrace();
                            }

                            CustomAdapter adapter = new CustomAdapter(getContext(), R.layout.home_listview_cell, homeFeedPostArray);
                            mHomeFeed.setAdapter(adapter);

                        }


                    }else{
                        Log.e("TAG", "Not User Post!");
                    }
                }

                Log.e("TAG", "Post Amounts: " + homeFeedPostArray.size());

                CustomAdapter adapter = new CustomAdapter(getContext(), R.layout.home_listview_cell, homeFeedPostArray);
                mHomeFeed.setAdapter(adapter);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });

    }

    public void loadAuthorProfile() {
        // Get a reference to our Users
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference ref = database.getReference("users").child(mAuth.getCurrentUser().getUid());


        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if (dataSnapshot.child("profileImageUrl").getValue() != null){
                    profileImageUrl = Objects.requireNonNull(dataSnapshot.child("profileImageUrl").getValue()).toString();
                }

                if (dataSnapshot.child("fullname").getValue() != null){
                    author = Objects.requireNonNull(dataSnapshot.child("fullname").getValue()).toString();
                }

                Log.e("TAG","Current Author: " + author);
                Log.e("TAG", "Current ProfileImageUrl: " + profileImageUrl);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

}
