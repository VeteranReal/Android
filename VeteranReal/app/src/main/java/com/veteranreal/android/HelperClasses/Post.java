package com.veteranreal.android.HelperClasses;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

import com.google.firebase.auth.FirebaseUser;

import java.io.Serializable;
import java.net.URL;
import java.sql.Time;
import java.util.UUID;

public class Post implements Serializable {

    private String mPostDescription;
    private String mPostLocation;
    private String mPostUUID;
    private String mPostUID;
    private String mPostAuthor;
    private String mPostTime;
    private long mLikes;
    private URL mPostImage;
    private URL mPostProfileImage;


    public Post(String Description, String Location, URL Image, URL ProfileImage, String Author, String Time, String postUUID, long Likes, String postUID){
        mPostDescription = Description;
        mPostLocation = Location;
        mPostUUID = postUUID;
        mPostImage = Image;
        mPostAuthor = Author;
        mPostTime = Time;
        mLikes = Likes;
        mPostProfileImage = ProfileImage;
        mPostUID = postUID;
    }

    public String getDescription() {
        return mPostDescription;
    }

    public String getLocation() {
        return mPostLocation;
    }

    public String getUUID() {
        return mPostUUID;
    }

    public URL getImage() {
        return mPostImage;
    }

    public String getAuthor() {
        return mPostAuthor;
    }

    public void setAuthor(String AuthorName){
        mPostAuthor = AuthorName;
    }

    public String getTime() {
        return mPostTime;
    }

    public URL getProfileImage() {
        return mPostProfileImage;
    }

    public void setProfileImage(URL imageURL){
        mPostProfileImage = imageURL;
    }

    public long getLikes() {
        return mLikes;
    }

    public String getmPostUID() {
        return mPostUID;
    }
}
