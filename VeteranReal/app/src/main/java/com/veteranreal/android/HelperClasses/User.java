package com.veteranreal.android.HelperClasses;

public class User {

    private String mEmail;
    private String mFullname;
    private String mProfileImageUrl;

    public User(String email, String fullname, String profileImageUrl){
        mEmail = email;
        mFullname = fullname;
        mProfileImageUrl = profileImageUrl;
    }

    public String getEmail() {
        return mEmail;
    }

    public String getFullname() {
        return mFullname;
    }

    public String getProfileImageUrl() {
        return mProfileImageUrl;
    }
}
