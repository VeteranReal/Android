package com.veteranreal.android.SignInActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.veteranreal.android.HomeActivity.HomeActivity;
import com.veteranreal.android.R;

import java.io.IOException;
import java.net.URI;
import java.util.UUID;

public class SignUpActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private EditText name;
    private EditText email;
    private EditText password;
    private EditText confirmPassword;
    private ImageView profileImage;
    private static int PICK_IMAGE_REQUEST = 71;
    private FirebaseStorage storage;
    private StorageReference storageReference;
    private Uri filePath;
    private boolean customProfilePicture = false;
    private String firebaseImageString;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup_layout);

        ImageView signUp = findViewById(R.id.imageView_signup_button);
        name = findViewById(R.id.editText_name);
        email = findViewById(R.id.editText_email);
        password = findViewById(R.id.editText_password);
        profileImage = findViewById(R.id.imageView_choose_profile_image);
        mAuth = FirebaseAuth.getInstance();
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();

        TextView logIn = findViewById(R.id.textView_already_have_account);

        logIn.setOnClickListener(new TextView.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signInIntent = new Intent(SignUpActivity.this, SignInActivity.class);
                startActivity(signInIntent);
            }
        });

        profileImage.setOnClickListener(new ImageView.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
            }
        });

        signUp.setOnClickListener(new ImageView.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (customProfilePicture){
                    if(name.getText().toString().length() > 3){
                        if (email.getText().toString().toLowerCase().contains("@gmail.com") || email.getText().toString().toLowerCase().contains("@yahoo.com") || email.getText().toString().toLowerCase().contains("@fullsail.com") || email.getText().toString().toLowerCase().contains("@fullsail.edu")) {
                            if (password.getText().toString().length() >= 6) {
                                Log.e("Email", email.getText().toString());

                                createUserAccount(email.getText().toString(), name.getText().toString(), password.getText().toString());
                            }else{
                                Toast.makeText(SignUpActivity.this, "Password Length failed.",
                                        Toast.LENGTH_SHORT).show();
                            }
                        }else {
                            Toast.makeText(SignUpActivity.this, "Must be a valid Email!",
                                    Toast.LENGTH_SHORT).show();
                        }
//                Intent signInIntent = new Intent(SignUpActivity.this, SignInActivity.class);
//                startActivity(signInIntent);
                    }else{
                        Toast.makeText(SignUpActivity.this, "Can't leave name field blank!",
                                Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(SignUpActivity.this, "Must choose profile picture.",
                            Toast.LENGTH_SHORT).show();
                }
            }

        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK
                && data != null && data.getData() != null )
        {
            filePath = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                profileImage.setImageBitmap(bitmap);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            customProfilePicture = true;
        }
    }

    private void createUserAccount(final String email, final String name, final String password){

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("TAG", "createUserWithEmail:success");

                            if (customProfilePicture){
                                uploadImage();
                            }

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("TAG", "createUserWithEmail:failure", task.getException());
                            Toast.makeText(SignUpActivity.this, "The Email is Already Tied to an Existing Account within Veteran Real.",
                                    Toast.LENGTH_SHORT).show();

                        }
                    }
                });
    }

    private void uploadImage() {

        if(filePath != null)
        {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Uploading...");
            progressDialog.show();

            final StorageReference ref = storageReference.child("profile_image_test/"+ mAuth.getCurrentUser().getUid());
            firebaseImageString = ref.getDownloadUrl().toString();

            Log.e("TAG" , "image url: " + ref.getDownloadUrl());
            ref.putFile(filePath)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            progressDialog.dismiss();

                            FirebaseUser user = mAuth.getCurrentUser();

                            FirebaseDatabase database = FirebaseDatabase.getInstance();
                            final DatabaseReference myRef = database.getReference("users").child(user.getUid());

                            myRef.child("fullname").setValue(name.getText().toString());
                            myRef.child("email").setValue(email.getText().toString());

                            ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    Log.d("TAG", "onSuccess: uri= " + uri.toString());
                                    myRef.child("profileImageUrl").setValue(uri.toString());
                                }
                            });

                            Toast.makeText(SignUpActivity.this, "Account Uploaded", Toast.LENGTH_SHORT).show();

                            Intent homeIntent = new Intent(SignUpActivity.this, HomeActivity.class);
                            startActivity(homeIntent);
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Toast.makeText(SignUpActivity.this, "Failed "+e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0*taskSnapshot.getBytesTransferred()/taskSnapshot
                                    .getTotalByteCount());
                            progressDialog.setMessage("Uploaded "+(int)progress+"%");
                        }
                    });
        }
    }

}
