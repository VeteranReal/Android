package com.veteranreal.android.UserProfileActivity.ChangePassActivity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.veteranreal.android.R;

public class ChangePassActivity extends AppCompatActivity {

    EditText email, password, newPassword;
    ImageView changePassword;
    FirebaseAuth mAuth;
    Snackbar goodChange, badChange;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_password_layout);

        mAuth = FirebaseAuth.getInstance();

        email = findViewById(R.id.editText_change_password_email);
        password = findViewById(R.id.editText_change_password_currentpass);
        newPassword = findViewById(R.id.editText_change_password_new_password);

        changePassword = findViewById(R.id.imageView_change_button);

        changePassword.setOnClickListener(new ImageView.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (newPassword.getText().length() >= 6){

                    AuthCredential credential = EmailAuthProvider
                            .getCredential(email.getText().toString(), password.getText().toString());

                    if (mAuth.getCurrentUser() != null){

                        mAuth.getCurrentUser().reauthenticate(credential)
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            mAuth.getCurrentUser().updatePassword(newPassword.getText().toString()).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    if (task.isSuccessful()) {

                                                        Toast.makeText(ChangePassActivity.this, "Password Updated!", Toast.LENGTH_LONG).show();

                                                        finish();

                                                    } else {
                                                        Toast.makeText(ChangePassActivity.this, "Error: Password Not Updated!", Toast.LENGTH_LONG).show();
                                                    }
                                                }
                                            });
                                        } else {
                                            Toast.makeText(ChangePassActivity.this, "Authorization Failed!", Toast.LENGTH_LONG).show();
                                        }
                                    }
                                });

                    }

                }else{
                    Toast.makeText(ChangePassActivity.this, "New Password must be 6 characters or longer!", Toast.LENGTH_LONG).show();
                }
            }
        });


    }
}
