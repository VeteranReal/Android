package com.veteranreal.android.SignInActivity.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.veteranreal.android.R;

public class SignInHomeFragment extends Fragment {

    SignInHomeListener mListener;

    public static SignInHomeFragment newInstance() {

        Bundle args = new Bundle();

        SignInHomeFragment fragment = new SignInHomeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static interface SignInHomeListener{

        public void signIn();
        public void signUp();
        public void orgReq();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof SignInHomeListener){
            mListener = (SignInHomeListener) context;
        }

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.signin_default_layout, container, false);

        ImageView signIn = rootView.findViewById(R.id.imageView_sign_in_button_signin);

        signIn.setOnClickListener(new ImageView.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.signIn();
            }
        });

        ImageView signUp = rootView.findViewById(R.id.imageView_sign_up_button);

        signUp.setOnClickListener(new ImageView.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.signUp();
            }
        });

        ImageView orgRequest = rootView.findViewById(R.id.imageView_org_request_button);

        return rootView;
    }
}
