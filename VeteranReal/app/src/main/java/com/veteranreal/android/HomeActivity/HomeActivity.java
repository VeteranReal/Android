package com.veteranreal.android.HomeActivity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.veteranreal.android.HelperClasses.BottomNavigationViewHelper;
import com.veteranreal.android.HomeActivity.Fragments.DiscoverFeedFragment;
import com.veteranreal.android.HomeActivity.Fragments.HomeFeedFragment;
import com.veteranreal.android.HomeActivity.Fragments.MessageHubFragment;
import com.veteranreal.android.HomeActivity.Fragments.NewsFeedFragment;
import com.veteranreal.android.HomeActivity.Fragments.NotificationFeedFragment;
import com.veteranreal.android.HomeActivity.Fragments.PostCreationFragment;
import com.veteranreal.android.HomeActivity.Fragments.ProfileFragment;
import com.veteranreal.android.R;

public class HomeActivity extends AppCompatActivity implements PostCreationFragment.PostCreationListener, HomeFeedFragment.HomeFeedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_activity_layout);
        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.navigation);

        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);

        bottomNavigationView.setOnNavigationItemSelectedListener
                (new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        Fragment selectedFragment = null;
                        switch (item.getItemId()) {
                            case R.id.Home:
                                selectedFragment = HomeFeedFragment.newInstance();
                                break;
                            case R.id.Discover:
                                selectedFragment = DiscoverFeedFragment.newInstance();
                                break;
                            case R.id.News:
                                selectedFragment = NewsFeedFragment.newInstance();
                                break;
                            case R.id.Notification:
                                selectedFragment = NotificationFeedFragment.newInstance();
                                break;
                            case R.id.Profile:
                                selectedFragment = ProfileFragment.newInstance();
                                break;
                        }
                        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.frame_layout_home_activity, selectedFragment);
                        transaction.commit();
                        return true;
                    }
                });

        //Manually displaying the first fragment - one time only
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout_home_activity, HomeFeedFragment.newInstance());
        transaction.commit();

        //Used to select an item programmatically
        //bottomNavigationView.getMenu().getItem(2).setChecked(true);
    }

    @Override
    public void sharePost() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frame_layout_home_activity, HomeFeedFragment.newInstance())
                .commit();

    }

    @Override
    public void createPost() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frame_layout_home_activity, PostCreationFragment.newInstance())
                .commit();
    }

    @Override
    public void messageHub() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frame_layout_home_activity, MessageHubFragment.newInstance())
                .commit();
    }
}
