package com.veteranreal.android.SignInActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.service.autofill.FillEventHistory;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.veteranreal.android.R;

public class OrgReqActivity extends AppCompatActivity {

    EditText orgName, orgEmail;
    ImageView createOrgButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.organization_access_layout);

        TextView signUp = findViewById(R.id.textView_not_org);

        orgEmail = findViewById(R.id.editText_organization_email);
        orgName = findViewById(R.id.editText_organization_name);

        createOrgButton = findViewById(R.id.imageView_organization_request_sumbit);

        createOrgButton.setOnClickListener(new ImageView.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (orgName.getText().length() > 0 && orgEmail.getText().length() > 0){

                    String subject = "Organization Access Request eMail";
                    String text = "Please Contact me in Reference to Creating an Orginization Account on Veteran Real App. Please Enter Org Contact Information Below. Please Include - Org Name, Org Email, Phone Number and Name of Person Requesting Access "
                            + "\n\n Org Name: " + orgName.getText().toString()
                            + "\n\n Org Contact Email: " + orgEmail.getText().toString()
                            + "\n\n Contact Phone Number: "
                            + "\n\n Name of Person Requesting Access: ";

                    Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                            "mailto","orgrequest@veteranreal.org", null));
                    intent.putExtra(Intent.EXTRA_SUBJECT, subject);
                    intent.putExtra(Intent.EXTRA_TEXT, text);
                    startActivity(Intent.createChooser(intent, "Choose an Email client :"));

                    finish();

                }else{
                    Toast.makeText(OrgReqActivity.this, "Must not leave any text fields blank!", Toast.LENGTH_SHORT).show();
                }

            }
        });

        signUp.setOnClickListener(new TextView.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signUpIntent = new Intent(OrgReqActivity.this, SignUpActivity.class);
                startActivity(signUpIntent);
            }
        });

    }
}
