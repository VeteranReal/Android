package com.veteranreal.android.HelperClasses;

public class PostComment {

        private String mUsername;
        private String mCommentText;
        private String mUUID;
        private String mCommentUID;

        public PostComment(String mUsername, String mCommentText, String mUUID, String mCommentUID) {
            this.mUsername = mUsername;
            this.mCommentText = mCommentText;
            this.mUUID = mUUID;
            this.mCommentUID = mCommentUID;
        }


        public String getmCommentUID() {
            return mCommentUID;
        }

        public String getmUUID() {
            return mUUID;
        }

        public String getmCommentText() {
            return mCommentText;
        }

        public String getmUsername() {
            return mUsername;
        }
    }

