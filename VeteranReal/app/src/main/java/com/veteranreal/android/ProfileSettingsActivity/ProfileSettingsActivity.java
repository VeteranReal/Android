package com.veteranreal.android.ProfileSettingsActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.veteranreal.android.HomeActivity.HomeActivity;
import com.veteranreal.android.R;
import com.veteranreal.android.SignInActivity.ResetPassActivity;
import com.veteranreal.android.SignInActivity.SignInHomeActivity;
import com.veteranreal.android.SignInActivity.SignUpActivity;
import com.veteranreal.android.UserProfileActivity.ChangePassActivity.ChangePassActivity;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Objects;

public class ProfileSettingsActivity extends AppCompatActivity {

    ImageView profileImage, logout_button, changePassButton;
    EditText name, branch, about;
    boolean customProfileImage = false;
    FirebaseAuth mAuth;
    private static int PICK_IMAGE_REQUEST = 71;
    private FirebaseStorage storage;
    private StorageReference storageReference;
    private Uri filePath;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_profile_settings_layout);


        //Firebase References
        mAuth = FirebaseAuth.getInstance();
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference ref = database.getReference("users").child(Objects.requireNonNull(mAuth.getCurrentUser()).getUid());
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();

        //image views
        profileImage = findViewById(R.id.imageView_choose_profile_image);
        logout_button = findViewById(R.id.imageView_profile_settings_logout);
        changePassButton = findViewById(R.id.imageView_change_password);

        logout_button.setOnClickListener(new ImageView.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent HomeScreenIntent = new Intent(ProfileSettingsActivity.this, SignInHomeActivity.class);
                mAuth.signOut();
                startActivity(HomeScreenIntent);
                finish();

            }
        });


        profileImage.setOnClickListener(new ImageView.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
            }
        });


        changePassButton.setOnClickListener(new ImageView.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent resetPassIntent = new Intent(ProfileSettingsActivity.this, ChangePassActivity.class);
                startActivity(resetPassIntent);

            }
        });

        //EditText's
        name = findViewById(R.id.editText_profile_settings_name);
        branch = findViewById(R.id.editText_profile_settings_branch);
        about = findViewById(R.id.editText_profile_settings_about);

        //Toolbar
        Toolbar toolbar = findViewById(R.id.toolbar_New_Post);

        TextView saveButton = toolbar.findViewById(R.id.textView_profile_settings_save_button);

        saveButton.setOnClickListener(new TextView.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (name.getText().length() > 0){

                    if (branch.getText().length() > 0){

                        if (about.getText().length() > 0){

                            Log.e("Tag","Updating Database");

                            ref.child("fullname").setValue(name.getText().toString());
                            ref.child("branch").setValue(branch.getText().toString());
                            ref.child("description").setValue(about.getText().toString());

                            if (customProfileImage){

                                uploadImage();

                            }else{
                                ProfileSettingsActivity.this.finish();
                            }



                        }

                    }

                }

            }
        });


        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                try {
                    Glide.with(ProfileSettingsActivity.this).load(new URL(Objects.requireNonNull(dataSnapshot.child("profileImageUrl").getValue()).toString())).into(profileImage);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }

                name.setText(dataSnapshot.child("fullname").getValue().toString());

                if (dataSnapshot.child("branch").getValue() != null){
                    branch.setText(dataSnapshot.child("branch").getValue().toString());
                }else{
                    branch.setText("Branch Not Defined");
                }

                if (dataSnapshot.child("description").getValue() != null){
                    about.setText(Objects.requireNonNull(dataSnapshot.child("description").getValue()).toString());
                }else{
                    about.setText("No Description Defined");
                }




            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void uploadImage() {

        if(filePath != null)
        {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Uploading...");
            progressDialog.show();

            final StorageReference ref = storageReference.child("profile_image_test/"+ mAuth.getCurrentUser().getUid());
//            firebaseImageString = ref.getDownloadUrl().toString();
//
//            Log.e("TAG" , "image url: " + ref.getDownloadUrl());
            ref.putFile(filePath)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            progressDialog.dismiss();


                            FirebaseDatabase database = FirebaseDatabase.getInstance();
                            final DatabaseReference myRef = database.getReference("users").child(mAuth.getCurrentUser().getUid());

                            ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    Log.d("TAG", "onSuccess: uri= " + uri.toString());
                                    myRef.child("profileImageUrl").setValue(uri.toString());
                                }
                            });

                            Toast.makeText(ProfileSettingsActivity.this, "Account Updated", Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Toast.makeText(ProfileSettingsActivity.this, "Failed "+e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0*taskSnapshot.getBytesTransferred()/taskSnapshot
                                    .getTotalByteCount());
                            progressDialog.setMessage("Uploaded "+(int)progress+"%");
                        }
                    });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK
                && data != null && data.getData() != null )
        {
            filePath = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                profileImage.setImageBitmap(bitmap);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            customProfileImage = true;
        }
    }
}
