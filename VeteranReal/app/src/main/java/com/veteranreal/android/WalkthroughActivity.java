package com.veteranreal.android;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.veteranreal.android.SignInActivity.SignInHomeActivity;

public class WalkthroughActivity extends AppCompatActivity {

    private int current_screen = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.walkthrough_layout_001);

        final ImageView walkthrough = findViewById(R.id.imageView_walkthrough);
        walkthrough.setOnClickListener(new ImageView.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (current_screen == 1){
                    walkthrough.setImageDrawable(getDrawable(R.drawable.walkthrough_002));
                    current_screen++;
                }else if (current_screen == 2){
                    walkthrough.setImageDrawable(getDrawable(R.drawable.walkthrough_003));
                    current_screen++;
                }else if (current_screen == 3){
                    Intent signinIntent = new Intent(WalkthroughActivity.this, SignInHomeActivity.class);
                    startActivity(signinIntent);
                    finish();
                }
            }
        });

    }
}
