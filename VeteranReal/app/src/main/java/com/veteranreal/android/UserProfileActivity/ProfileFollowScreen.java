package com.veteranreal.android.UserProfileActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.veteranreal.android.HelperClasses.Notification;
import com.veteranreal.android.HomeActivity.Fragments.NotificationFeedFragment;
import com.veteranreal.android.R;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Objects;

public class ProfileFollowScreen extends AppCompatActivity {

    ListView listView;
    TextView pageTitle;
    FirebaseAuth mAuth;
    ArrayList<Notification> data;
    boolean followers;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_follow_screen_layout);

        data = new ArrayList<Notification>();

        mAuth = FirebaseAuth.getInstance();

        listView = findViewById(R.id.listView_follow_screen);


        Toolbar toolbar = findViewById(R.id.toolbar_New_Post);

        pageTitle = (TextView) toolbar.findViewById(R.id.toobar_text_title);

        pageTitle.setText(Objects.requireNonNull(getIntent().getExtras()).getString("TitleFollowScreen"));

        listView.setOnItemClickListener(new ListView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent userProfile = new Intent(ProfileFollowScreen.this, UserProfileActivity.class);

                userProfile.putExtra("currentUser", data.get(position));

                startActivity(userProfile);

            }
        });


        if (pageTitle.getText().toString().equals("Followers")){
            Log.e("TAG","Showing Your Followers!");
            followers = true;
        }else if (pageTitle.getText().toString().equals("Following")){
            Log.e("TAG", "Showing Who you Follow!");
            followers  = false;
        }else{
            Log.e("TAG", "No Title Grabbed Yet!");
        }


        loadFollowData();

    }

    public void loadFollowData(){

        FirebaseUser user = mAuth.getCurrentUser();

        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref;

        if (followers){
            ref = database.getReference("followers").child(user.getUid());
        }else {
             ref = database.getReference("following").child(user.getUid());
        }


        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                ArrayList<String> UUIDArray = new ArrayList<>();

                for (DataSnapshot snapshot: dataSnapshot.getChildren()){

                    Log.e("TAG", "Snapshot: " + snapshot);

                    UUIDArray.add(snapshot.getKey());

                }

                loadUserProfiles(UUIDArray);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    public void loadUserProfiles(ArrayList<String> uuids){

        final FirebaseDatabase database = FirebaseDatabase.getInstance();

        for (String uuid: uuids){

            DatabaseReference ref = database.getReference().child("users").child(uuid);

            final String currentUUID = uuid;

            ref.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    data.add(new Notification(dataSnapshot.child("fullname").getValue().toString(), currentUUID, "Follow"));

                    Log.e("TAG", "Current data Length : " + data.size());

                    listView.setAdapter(new CustomAdapter(ProfileFollowScreen.this, R.layout.notification_feed_cell, data));

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

        }
    }


    class CustomAdapter extends ArrayAdapter<Notification>
    {

        private int listItemLayout;

        public CustomAdapter(Context context, int layoutId, ArrayList<Notification> array)
        {
            super(context, layoutId, array);

            listItemLayout  = layoutId;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {


            final Notification currentItem = getItem(position);
            final ViewHolder vh;

            if(convertView == null)
            {
                vh = new ViewHolder();
                LayoutInflater inflater = LayoutInflater.from(getContext());
                convertView = inflater.inflate(listItemLayout, parent, false);

                //inflate custom layout
                vh.followText = convertView.findViewById(R.id.textView_notification_text);

                convertView.setTag(vh);
            }
            else
            {
                vh = (ViewHolder) convertView.getTag();
            }

                vh.followText.setText(Objects.requireNonNull(currentItem).getmName());


            return convertView;
        }

        private class ViewHolder
        {
            TextView followText;
            ImageView profile_image;
        }

    }



}
