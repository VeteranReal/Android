package com.veteranreal.android.HomeActivity.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.SearchView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.veteranreal.android.HelperClasses.Post;
import com.veteranreal.android.PostDetailActivity.PostDetailActivity;
import com.veteranreal.android.R;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;

public class DiscoverFeedFragment extends Fragment {

    ArrayList<Post> posts;
    private String profileImageUrl;
    private String author;
    private FirebaseAuth mAuth;
    private GridView grid;
    private GridAdapter adapter;
    private int currentPostID;
    SearchView discoverSearch;

    public static DiscoverFeedFragment newInstance() {

        Bundle args = new Bundle();

        DiscoverFeedFragment fragment = new DiscoverFeedFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.discover_feed_layout, container, false);

        posts = new ArrayList<>();

        discoverSearch = rootView.findViewById(R.id.searchView_discover);

        currentPostID = 1;

        loadPosts();

        grid = rootView.findViewById(R.id.gridView_discover_feed);

        adapter = new GridAdapter();

        grid.setAdapter(adapter);


        return rootView;
    }

    private void loadPosts(){


        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference("posts");
        mAuth = FirebaseAuth.getInstance();

        posts.clear();
        //adapter.notifyDataSetChanged();


        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                URL postImage = null;


                for (DataSnapshot snapshot: dataSnapshot.getChildren()){
                    Log.e("TAG", "Found post");

                    try {
                        if (snapshot.child("photoUrl").getValue() != null){

                            postImage = new URL(Objects.requireNonNull(snapshot.child("photoUrl").getValue()).toString());

                        }else{
                            return;
                        }
                        String uid;
                        if (snapshot.child("uid").getValue() != null){
                             uid = snapshot.child("uid").getValue().toString();
                        }else {
                            uid = "n/a";
                        }
                        String location;
                        if (snapshot.child("location").getValue() == null){
                            location = "N/A";
                        }else{
                            location = snapshot.child("location").getValue().toString();
                        }

                        Log.e("Tag", snapshot.getKey());

                        if (snapshot.child("likeCount").getValue() != null){
                            addToArray(snapshot.child("caption").getValue().toString(), location, postImage, (long) snapshot.child("likeCount").getValue(), uid, snapshot.getKey());
                        }else{
                            addToArray(snapshot.child("caption").getValue().toString(), location, postImage, 0, uid, snapshot.getKey());
                        }


                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }

                }

                //UPDATE GRIDVIEW ADAPTER HERE?
                grid.setAdapter(adapter);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void addToArray(final String description, final String location, final URL postImage, final long likes, final String authorUid, final String postUUID){

        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference("users").child(authorUid);

        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                try {
                    URL profileImage = new URL(dataSnapshot.child("profileImageUrl").getValue().toString());

                    Post newPost = new Post(description, location, postImage, profileImage, dataSnapshot.child("fullname").getValue().toString(), "30 min ago",  postUUID, likes, authorUid);

                    posts.add(newPost);

                    adapter.notifyDataSetChanged();

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

    private class GridAdapter extends BaseAdapter{

        public GridAdapter() {

        }

        @Override
        public int getCount() {
            return posts.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            final ViewHolder holder;

            if(convertView == null){
                convertView = getLayoutInflater().inflate(R.layout.grid_view_cell_layout, parent, false);

                holder = new ViewHolder();
                holder.imageView = convertView.findViewById(R.id.imageView_cellImage);

                convertView.setTag(holder);
            }else{
                holder = (ViewHolder) convertView.getTag();
            }

            Glide.with(getContext()).load(posts.get(position).getImage()).apply(RequestOptions.centerCropTransform()).into(holder.imageView);

            holder.imageView.setOnClickListener(new ImageView.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent postDetailIntent = new Intent(getActivity(), PostDetailActivity.class);

                    postDetailIntent.putExtra("ClickedPost", posts.get(position));

                        postDetailIntent.putExtra("liked", false);

                    //postDetailIntent.putExtra("likes", likes);

                    startActivity(postDetailIntent);
                }
            });


            return convertView;
        }

    }

    private static class ViewHolder {
        ImageView imageView;
    }



}
