package com.veteranreal.android.HomeActivity.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.veteranreal.android.HomeActivity.HomeActivity;
import com.veteranreal.android.MessageActivity.MessageActivity;
import com.veteranreal.android.R;

public class MessageHubFragment extends Fragment {

    public static MessageHubFragment newInstance() {

        Bundle args = new Bundle();

        MessageHubFragment fragment = new MessageHubFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.message_hub_layout, container, false);

        ImageView messageRoom = rootView.findViewById(R.id.imageView_message_room_1);

        messageRoom.setOnClickListener(new ImageView.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent messageIntent = new Intent(getActivity(), MessageActivity.class);
                startActivity(messageIntent);

            }
        });

        return rootView;

    }
}
