package com.veteranreal.android.PostDetailActivity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.veteranreal.android.HelperClasses.PostComment;
import com.veteranreal.android.R;

import java.util.ArrayList;
import java.util.Objects;
import java.util.UUID;

public class CreateCommentActivity extends AppCompatActivity {

    ListView commentListView;
    Button sendBtn;
    EditText commentText;
    private ArrayList<PostComment> commentsArray;
    String username = "";
    FirebaseAuth mAuth = FirebaseAuth.getInstance();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.comment_screen_layout);

        commentListView = findViewById(R.id.listView_comment_screen);
        sendBtn = findViewById(R.id.button_send);
        commentText = findViewById(R.id.editText_comment_text);

        commentsArray = new ArrayList<>();

        final String postUID = getIntent().getStringExtra("postUID");

        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference commentsRef = database.getReference("comments").child(postUID);

        commentsRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for(DataSnapshot comment: dataSnapshot.getChildren()){

                    Log.e("TAG", "Comment Text: " + comment.child("commentText").getValue());

                    commentsArray.add(new PostComment(comment.child("commentersName").getValue().toString(),comment.child("commentText").getValue().toString(),comment.child("UUID").getValue().toString(),comment.child("commentUID").getValue().toString()));


                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        Log.e("TAG", "onCreate: " + postUID);

        final CustomAdapter adapter = new CustomAdapter(this, R.layout.comment_listview_cell, commentsArray);

        commentListView.setAdapter(adapter);

        final DatabaseReference myUserRef = database.getReference("users").child(mAuth.getCurrentUser().getUid());

        myUserRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                username = dataSnapshot.child("fullname").getValue().toString();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        sendBtn.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (commentText.getText().length() > 0){
                    Log.e("TAG","Valid Comment");

                    final UUID randomCommentID = UUID.randomUUID();

                    final DatabaseReference myRef = database.getReference("comments").child(postUID);

                    myRef.child("-LN" + randomCommentID).child("UUID").setValue(mAuth.getCurrentUser().getUid());

                    myRef.child("-LN" + randomCommentID).child("commentersName").setValue(username);
                    myRef.child("-LN" + randomCommentID).child("commentText").setValue(commentText.getText().toString());
                    myRef.child("-LN" + randomCommentID).child("commentUID").setValue("-LN" + randomCommentID);

                    commentsArray.add(new PostComment(username, commentText.getText().toString(), mAuth.getCurrentUser().getUid(),"-LN" + randomCommentID));
                    commentListView.setAdapter(adapter);

                }

            }
        });

    }


    private class CustomAdapter extends ArrayAdapter<PostComment> {

        private int listItemLayout;

        public CustomAdapter(@NonNull Context context, int layoutId, @NonNull ArrayList<PostComment> commentsArray) {
            super(context, layoutId, commentsArray);

            listItemLayout = layoutId;

        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            final PostComment currentComment = getItem(position);
            final ViewHolder vh;

            if (convertView == null){
                vh = new ViewHolder();
                LayoutInflater inflater = LayoutInflater.from(getContext());
                convertView = inflater.inflate(listItemLayout, parent, false);

                vh.CommenterName = convertView.findViewById(R.id.textView_comment_username);
                vh.CommentText = convertView.findViewById(R.id.textView_comment_comment);
                vh.ProfileImage = convertView.findViewById(R.id.imageView_comment_profile_image);

                convertView.setTag(vh);

            }else{
                vh = (ViewHolder) convertView.getTag();
            }

            vh.CommenterName.setText(currentComment.getmUsername());
            vh.CommentText.setText(currentComment.getmCommentText());

            final FirebaseDatabase database = FirebaseDatabase.getInstance();
            final DatabaseReference ref = database.getReference("users").child(currentComment.getmUUID());


            ref.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    if (dataSnapshot.child("profileImageUrl").getValue() != null){
                        //profileImageUrl = Objects.requireNonNull(dataSnapshot.child("profileImageUrl").getValue()).toString();
                        Glide.with(getContext()).load(dataSnapshot.child("profileImageUrl").getValue().toString()).into(vh.ProfileImage);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });




            return convertView;
        }

        private class ViewHolder{
            TextView CommenterName, CommentText;
            ImageView ProfileImage;
        }


    }

}
