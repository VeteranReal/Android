package com.veteranreal.android.HelperClasses;

import java.io.Serializable;

public class Notification implements Serializable {

    private String mName;
    private String mUUID;
    private String mType;

    public Notification(String mName, String mUUID, String mType) {
        this.mName = mName;
        this.mUUID = mUUID;
        this.mType = mType;
    }

    public String getmName() {
        return mName;
    }

    public String getmUUID() {
        return mUUID;
    }

    public String getmType() {
        return mType;
    }
}
