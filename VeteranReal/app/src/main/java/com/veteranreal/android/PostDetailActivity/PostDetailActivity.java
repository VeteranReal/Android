package com.veteranreal.android.PostDetailActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.veteranreal.android.HelperClasses.Notification;
import com.veteranreal.android.HelperClasses.Post;
import com.veteranreal.android.HelperClasses.PostComment;
import com.veteranreal.android.HomeActivity.Fragments.HomeFeedFragment;
import com.veteranreal.android.HomeActivity.HomeActivity;
import com.veteranreal.android.R;
import com.veteranreal.android.UserProfileActivity.UserProfileActivity;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static java.security.AccessController.getContext;

public class PostDetailActivity extends AppCompatActivity {

    TextView name, location, timeStamp, like_count, post_description;
    ImageView profile_image, post_image, like_image, comment_image;
    private FirebaseAuth mAuth;
    private Post currentPost;
    private Boolean isLiked = false;
    private long likes;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.post_details_layout);

        mAuth = FirebaseAuth.getInstance();

        name = findViewById(R.id.textView_post_user_name);
        location = findViewById(R.id.textView_post_location);
//        timeStamp = findViewById(R.id.textView_post_time);
        like_count = findViewById(R.id.textView_post_like_count);
        post_description = findViewById(R.id.textView_post_description);

        profile_image = findViewById(R.id.imageView_post_profile_image);
        post_image = findViewById(R.id.imageView_post_image);
        like_image = findViewById(R.id.imageView_post_like);
        comment_image = findViewById(R.id.imageView_post_comment);

        comment_image.setOnClickListener(new ImageView.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent postCommentsIntent = new Intent(PostDetailActivity.this, CreateCommentActivity.class);
                postCommentsIntent.putExtra("postUID", currentPost.getUUID());
                startActivity(postCommentsIntent);
            }
        });

        currentPost = (Post) getIntent().getSerializableExtra("ClickedPost");
        likes = getIntent().getLongExtra("likes", 0);
        isLiked = getIntent().getBooleanExtra("liked", false);

        name.setText(currentPost.getAuthor());

        name.setOnClickListener(new TextView.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!currentPost.getmPostUID().equals(mAuth.getCurrentUser().getUid())){
                    Intent openUserProfileIntent = new Intent(PostDetailActivity.this, UserProfileActivity.class);
                    openUserProfileIntent.putExtra("currentUser", new Notification(currentPost.getAuthor(),currentPost.getmPostUID(),"Follow"));
                    startActivity(openUserProfileIntent);
                }else{
                    Toast.makeText(PostDetailActivity.this, "Can't Open Your Own Profile!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        location.setText(currentPost.getLocation());
//        timeStamp.setText(currentPost.getTime());
        post_description.setText(currentPost.getDescription());
        //like_count.setText(String.valueOf(currentPost.getLikes()));
        like_count.setText(String.valueOf(likes));
//        comment_count.setText("0");
        Glide.with(PostDetailActivity.this).load(currentPost.getProfileImage()).into(profile_image);
        Glide.with(PostDetailActivity.this).load(currentPost.getImage()).into(post_image);
//        if (isLiked){
//            Glide.with(PostDetailActivity.this).load(R.drawable.like_image_active).into(like_image);
//        }

        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference postRef = database.getReference("posts").child(currentPost.getUUID());

        postRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {

                for (DataSnapshot like : dataSnapshot.child("likes").getChildren()) {

                    if (Objects.requireNonNull(like.getKey()).equals(Objects.requireNonNull(mAuth.getCurrentUser()).getUid())) {
                        Glide.with(PostDetailActivity.this).load(R.drawable.like_image_active).into(like_image);
                        isLiked = true;
                    }
                    likes = (long) dataSnapshot.child("likeCount").getValue();
                    like_count.setText(String.valueOf(likes));

                }

                if (dataSnapshot.child("uid").getValue().toString().equals(mAuth.getCurrentUser().getUid())){

                    Button delete_button = findViewById(R.id.button_delete_post);
                    delete_button.setVisibility(View.VISIBLE);

                    delete_button.setOnClickListener(new Button.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            postRef.removeValue();

                            finish();

                        }
                    });

                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        like_image.setOnClickListener(new ImageView.OnClickListener() {
            @Override
            public void onClick(View v) {
                final FirebaseDatabase database = FirebaseDatabase.getInstance();
                final DatabaseReference postRef = database.getReference("posts").child(currentPost.getUUID());

                postRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        //likes = (long) dataSnapshot.child("likeCount").getValue();

                        if (!isLiked){
                            likes++;
                            postRef.child("likes").child(mAuth.getCurrentUser().getUid()).setValue("true");
                            postRef.child("likeCount").setValue(likes);
                            like_image.setImageDrawable(getResources().getDrawable(R.drawable.like_image_active));
                            like_count.setText(String.valueOf(likes));
                            isLiked = true;
                        }else{

                            if (likes > 0){
                                likes--;
                            }
                            postRef.child("likes").child(mAuth.getCurrentUser().getUid()).removeValue();
                            like_image.setImageDrawable(getResources().getDrawable(R.drawable.like));
                            postRef.child("likeCount").setValue(likes);
                            like_count.setText(String.valueOf(likes));
                            isLiked = false;
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
        });


    }
}

