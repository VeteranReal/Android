package com.veteranreal.android.UserProfileActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.veteranreal.android.HelperClasses.Notification;
import com.veteranreal.android.HelperClasses.Post;
import com.veteranreal.android.HomeActivity.Fragments.ProfileFragment;
import com.veteranreal.android.PostDetailActivity.PostDetailActivity;
import com.veteranreal.android.R;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Objects;

public class UserProfileActivity extends AppCompatActivity {

    TextView name, branch, description;
    ImageView profileImage, follow_button;
    Notification currentNotificationUser;
    FirebaseAuth mAuth;
    Boolean isFollowing;
    GridView gridView;
    ArrayList<Post> profilePostArray;
    GridAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_profile_layout);

        gridView = findViewById(R.id.gridView_user_profile_layout);

        TextView empty = findViewById(R.id.user_profile_layout_empty);

        isFollowing = false;

        gridView.setEmptyView(empty);

        profilePostArray = new ArrayList<>();

        adapter = new GridAdapter();

        gridView.setAdapter(adapter);

        mAuth = FirebaseAuth.getInstance();

        currentNotificationUser = (Notification) getIntent().getSerializableExtra("currentUser");

        name = findViewById(R.id.textView_profile_user_name);
        branch = findViewById(R.id.textView_profile_military_branch);
        description = findViewById(R.id.textView_profile_description);

        profileImage = findViewById(R.id.imageView_profile_user_image);
        follow_button = findViewById(R.id.imageView_follow_button);

        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference ref = database.getReference("following").child(mAuth.getCurrentUser().getUid());


        follow_button.setOnClickListener(new ImageView.OnClickListener() {
            @Override
            public void onClick(View v) {

                final DatabaseReference ref2 = database.getReference("followers").child(currentNotificationUser.getmUUID());

                final DatabaseReference notificationRef = database.getReference("notification").child(mAuth.getCurrentUser().getUid());

                final DatabaseReference notificationRef2 = database.getReference("notification").child(currentNotificationUser.getmUUID());



                if (isFollowing){

                    follow_button.setImageDrawable(getResources().getDrawable(R.drawable.follow_button));

                    Log.e("TAG", "Following, Removing Now");

                    ref.child(currentNotificationUser.getmUUID()).removeValue();
                    ref2.child(mAuth.getCurrentUser().getUid()).removeValue();
                    notificationRef2.child(mAuth.getCurrentUser().getUid()).removeValue();

                    isFollowing = false;

                    checkFollowerStatus();


                }else{

                    Log.e("TAG", "Not Following, Adding Now");
                    follow_button.setImageDrawable(getResources().getDrawable(R.drawable.unfollow_button));

                    ref.child(currentNotificationUser.getmUUID()).setValue(true);

                    ref2.child(mAuth.getCurrentUser().getUid()).setValue(true);

                    notificationRef2.child(mAuth.getCurrentUser().getUid()).child("name");

                    checkFollowerStatus();
                }

            }
        });


        loadProfile();
        loadPosts();
        checkFollowerStatus();


    }

    public void loadProfile() {
        // Get a reference to our Users
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference ref = database.getReference("users").child(currentNotificationUser.getmUUID());
        final DatabaseReference postsRef = database.getReference("posts");


        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                try {
                    Glide.with(Objects.requireNonNull(UserProfileActivity.this)).load(new URL(Objects.requireNonNull(dataSnapshot.child("profileImageUrl").getValue()).toString())).into(profileImage);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }

                name.setText(Objects.requireNonNull(dataSnapshot.child("fullname").getValue()).toString());

                if (dataSnapshot.child("branch").getValue() != null){
                    branch.setText(Objects.requireNonNull(dataSnapshot.child("branch").getValue()).toString());
                }else{
                    branch.setText("Branch Not Defined");
                }

                if (dataSnapshot.child("description").getValue() != null){
                    description.setText(Objects.requireNonNull(dataSnapshot.child("description").getValue()).toString());
                }else{
                    description.setText("No Description Defined");
                }

                checkFollowerStatus();

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void loadPosts(){

        // Get a reference to our Users
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference ref = database.getReference("posts");

        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot snapshot: dataSnapshot.getChildren()){

                    if (Objects.requireNonNull(snapshot.child("uid").getValue()).toString().equals(Objects.requireNonNull(currentNotificationUser.getmUUID()))){
                        Log.e("Tag", "Found User Post! Adding to Array");


                        try {

                            URL postImage = new URL(snapshot.child("photoUrl").getValue().toString());

                            loadAuthorInformation(snapshot.child("caption").getValue().toString(),
                                    snapshot.child("location").getValue().toString(),
                                    postImage,
                                    "30 min ago",
                                    snapshot.child("postUUID").getValue().toString(),
                                    (long) snapshot.child("likeCount").getValue(), snapshot.child("uid").getValue().toString());

                            //profilePostArray.add(new Post(snapshot.child("caption").getValue().toString(), snapshot.child("location").getValue().toString(), , "", "", "", "", "", ""))
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        }
                    }

                }

                adapter.notifyDataSetChanged();

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void loadAuthorInformation(final String description, final String location, final URL postImage, final String time, final String postUUID, final long likes, final String authorUid){

        // Get a reference to our Users
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference ref = database.getReference("users").child(authorUid);

        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                try {

                    URL postProfileImage = new URL(dataSnapshot.child("profileImageUrl").getValue().toString());

                    profilePostArray.add(new Post(description, location, postImage, postProfileImage, dataSnapshot.child("fullname").getValue().toString(), time, postUUID, likes, authorUid));
                    adapter.notifyDataSetChanged();
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });



    }

    private class GridAdapter extends BaseAdapter {

        public GridAdapter() {

        }

        @Override
        public int getCount() {
            return profilePostArray.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            final ViewHolder holder;

            if(convertView == null){
                convertView = getLayoutInflater().inflate(R.layout.grid_view_cell_layout, parent, false);

                holder = new ViewHolder();
                holder.imageView = convertView.findViewById(R.id.imageView_cellImage);

                convertView.setTag(holder);
            }else{
                holder = (ViewHolder) convertView.getTag();
            }

            Glide.with(UserProfileActivity.this).load(profilePostArray.get(position).getImage()).apply(RequestOptions.centerCropTransform()).into(holder.imageView);

            holder.imageView.setOnClickListener(new ImageView.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent postDetailIntent = new Intent(UserProfileActivity.this, PostDetailActivity.class);

                    postDetailIntent.putExtra("ClickedPost", profilePostArray.get(position));

                    postDetailIntent.putExtra("liked", false);

                    //postDetailIntent.putExtra("likes", likes);

                    startActivity(postDetailIntent);
                }
            });


            return convertView;
        }

    }

    private static class ViewHolder {
        ImageView imageView;
    }

    private void checkFollowerStatus(){

        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference ref = database.getReference("following").child(mAuth.getCurrentUser().getUid());

        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot snapshot: dataSnapshot.getChildren()){

                    Log.e("TAG", snapshot.getKey());

                    if (Objects.equals(snapshot.getKey(), currentNotificationUser.getmUUID())) {
                        isFollowing = true;

                        Log.e("TAG", "Already Following");

                        follow_button.setImageDrawable(getResources().getDrawable(R.drawable.unfollow_button));

                        return;

                    }else{
                        isFollowing = false;
                    }

                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }


}
