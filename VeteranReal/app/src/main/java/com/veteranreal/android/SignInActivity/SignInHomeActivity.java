package com.veteranreal.android.SignInActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.veteranreal.android.R;
import com.veteranreal.android.SignInActivity.Fragments.SignInFragment;
import com.veteranreal.android.SignInActivity.Fragments.SignInHomeFragment;

public class SignInHomeActivity extends AppCompatActivity {

    @Override
    public void onBackPressed() {
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signin_default_layout);

        ImageView signIn = findViewById(R.id.imageView_sign_in_button_signin);

        signIn.setOnClickListener(new ImageView.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signInIntent = new Intent(SignInHomeActivity.this, SignInActivity.class);
                startActivity(signInIntent);
            }
        });

        ImageView signUp = findViewById(R.id.imageView_sign_up_button);

        signUp.setOnClickListener(new ImageView.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signUpIntent = new Intent(SignInHomeActivity.this, SignUpActivity.class);
                startActivity(signUpIntent);
            }
        });

        ImageView orgRequest = findViewById(R.id.imageView_org_request_button);

        orgRequest.setOnClickListener(new ImageView.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent orgReqIntent = new Intent(SignInHomeActivity.this, OrgReqActivity.class);
                startActivity(orgReqIntent);
            }
        });

        TextView forgotPassword = findViewById(R.id.textView_forgotten_password);

        forgotPassword.setOnClickListener(new TextView.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent resetPassIntent = new Intent(SignInHomeActivity.this, ResetPassActivity.class);
                startActivity(resetPassIntent);
            }
        });

    }
}
